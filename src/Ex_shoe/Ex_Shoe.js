import React, { Component } from "react";
import List_Shoe from "./List_Shoe";
import { shoeArr } from "./Data";
import Cart_Shoe from "./Cart_Shoe";
import Detail_Shoe from "./Detail_Shoe";

export default class Ex_Shoe extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    cart: [],
  };
  handleViewDetail = (shoe) => {
    this.setState({
      detailShoe: shoe,
    });
  };
  handleAddCart = (shoe) => {
    console.log("id", shoe.id);
    let cloneCart = [...this.state.cart];

    let index = cloneCart.findIndex((item) => {
      return shoe.id == item.id;
    });

    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    // nếu chưa có trong giỏ thì push and render
    // nếu đã có thì thêm số lượng thêm +1
    this.setState({
      cart: cloneCart,
    });
  };
  handleDelete = (id) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };
  handleQuantity = (id, luaChon) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    cloneCart[index].soLuong += luaChon;
    cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-6">
            <Cart_Shoe
              cart={this.state.cart}
              handleDelete={this.handleDelete}
              handleQuantity={this.handleQuantity}
            />
          </div>
          <div className="col-6">
            <List_Shoe
              list={this.state.shoeArr}
              handleViewDetail={this.handleViewDetail}
              handleAddCart={this.handleAddCart}
            />
          </div>
        </div>
        <div>
          <Detail_Shoe detailShoe={this.state.detailShoe} />
        </div>
      </div>
    );
  }
}
