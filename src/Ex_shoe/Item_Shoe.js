import React, { Component } from "react";

export default class Item_Shoe extends Component {
  render() {
    let { name, price, image } = this.props.dataItem;
    return (
      <div className="col-6 py-3">
        <div className="card text-center pb-3">
          <img
            style={{ width: "200px" }}
            className="card-img-top"
            src={image}
          />
          <div className="card-body">
            <h5 style={{ fontSize: "16px" }} className="card-title">
              {name}
            </h5>
            <p className="card-text">{price}</p>
          </div>
          <div>
            <button
              onClick={() => {
                this.props.handleViewDetail(this.props.dataItem);
              }}
              className="btn btn-warning"
            >
              View Detail
            </button>
            <button
              onClick={() => {
                this.props.handleAddCart(this.props.dataItem);
              }}
              className="btn btn-success"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}
