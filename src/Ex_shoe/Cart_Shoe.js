import React, { Component } from "react";

export default class Cart_Shoe extends Component {
  renderCart = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>
            <img style={{ width: "80px" }} src={item.image}></img>
          </td>
          <td>{item.name}</td>
          <td style={{ textAlign: "center" }} className="p-0">
            <button
              onClick={() => {
                this.props.handleQuantity(item.id, 1);
              }}
              style={{ width: "20px", padding: "8px 0" }}
              className="btn btn-success"
            >
              +
            </button>
            <strong className="px-2">{item.soLuong}</strong>
            <button
              onClick={() => {
                this.props.handleQuantity(item.id, -1);
              }}
              style={{ width: "20px", padding: "8px 0" }}
              className="btn btn-warning"
            >
              -
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
              className="btn text-danger border-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Image</th>
              <th>Name</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
        </table>
      </div>
    );
  }
}
