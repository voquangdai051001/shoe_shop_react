import React, { Component } from "react";

export default class Detail_Shoe extends Component {
  render() {
    let { name, alias, price, description, shortDescription, quantity, image } =
      this.props.detailShoe;
    return (
      <div>
        <div className="card">
          <img
            style={{
              width: "200px",
            }}
            className="card-img-top"
            src={image}
            alt="Card image cap"
          />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{price}</p>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">{alias}</li>
            <li className="list-group-item">{description}</li>
            <li className="list-group-item">{shortDescription}</li>
            <li className="list-group-item">{quantity}</li>
          </ul>
        </div>
      </div>
    );
  }
}

/***
 * {
  
  "detailShoe": {
    "id": 1,
    "name": "Adidas Prophere",
    "alias": "adidas-prophere",
    "price": 350,
    "description": "The adidas Primeknit upper wraps the foot with a supportive fit that enhances movement.\r\n\r\n",
    "shortDescription": "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
    "quantity": 995,
    "image": "http://svcy3.myclass.vn/images/adidas-prophere.png"
  }
}
 */
