import React, { Component } from "react";
import Item_Shoe from "./Item_Shoe";

export default class List_Shoe extends Component {
  renderListShoe = () => {
    return this.props.list.map((item) => {
      return (
        <Item_Shoe
          dataItem={item}
          handleViewDetail={this.props.handleViewDetail}
          handleAddCart={this.props.handleAddCart}
        />
      );
    });
  };

  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
